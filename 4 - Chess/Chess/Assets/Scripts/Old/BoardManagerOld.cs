﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManagerOld : MonoBehaviour {

    public GameObject movingPiece;
    private LinearMovement moveScript;
    private HorseMovement horseScript;

    public Vector3 currentTarget;

    public string currentName;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            if (GameObject.Find(name).tag == "Horse")
            {
                FindPieceHorse(currentName);
                MoveFoundHorsePiece(currentTarget);
            }
            else
            {
                FindPieceLinear(currentName);
                MoveFoundLinearPiece(currentTarget);
            }

            ResetPiece();
        }
	}

    void FindPieceLinear (string name)
    {
        movingPiece = GameObject.Find(name);
        moveScript = movingPiece.GetComponent<LinearMovement>();
    }

    void FindPieceHorse(string name)
    {
        movingPiece = GameObject.Find(name);
        horseScript = movingPiece.GetComponent<HorseMovement>();
    }

    void MoveFoundLinearPiece (Vector3 localTarget)
    {
        moveScript.target = localTarget;
        moveScript.moveTrigger = true;
    }

    void MoveFoundHorsePiece(Vector3 localTarget)
    {
        horseScript.target = localTarget;
        horseScript.moveTrigger = true;
    }

    void ResetPiece ()
    {
        movingPiece = null;
        moveScript = null;
        horseScript = null;
        currentTarget = Vector3.zero;
        currentName = "";
    }

}
