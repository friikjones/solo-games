﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

    public GameObject movingPiece;
    private LinearMovement moveScript;
    private HorseMovement horseScript;
    private bool isHorse;

    public Vector3 currentTarget;

    public string currentName;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            FindPiece(currentName);
            MoveFoundPiece(currentTarget);
            ResetPiece();
        }
	}

    void FindPiece (string name)
    {
        movingPiece = GameObject.Find(name);
        if(movingPiece.tag == "Horse")
        {
            horseScript = movingPiece.GetComponent<HorseMovement>();
            isHorse = true;
        }
        else
        {
            moveScript = movingPiece.GetComponent<LinearMovement>();
            isHorse = false;
        }
    }

    void MoveFoundPiece (Vector3 localTarget)
    {
        if(isHorse)
        {
            horseScript.target = localTarget;
            horseScript.moveTrigger = true;
        }
        else
        {
            moveScript.target = localTarget;
            moveScript.moveTrigger = true;
        }
        
    }

    void ResetPiece ()
    {
        movingPiece = null;
        moveScript = null;
        horseScript = null;
        currentTarget = Vector3.zero;
        currentName = "";
        isHorse = false;
    }

}
