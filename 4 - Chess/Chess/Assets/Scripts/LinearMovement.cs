﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearMovement : MonoBehaviour {

    //public variables
    public Vector3 target;
    public float speed;
    public int moveState;
    public bool moveTrigger;
    
    //private variables
    private bool moving;
    private Vector3 current;
    private Vector3 localTarget;


	void Start () {
        moving = false;
        moveTrigger = false;
        moveState = 0;
	}
	

	void Update () {

        //State machine for linear movement
        switch (moveState)
            {
                case 0: //Idle state
                    {
                        if (moveTrigger)
                            moveState = 1;
                    }
                    break;

                case 1: //Move up state
                    {
                        if(MoveRelative(Vector3.up))
                            moveState = 2;
                    }
                    break;

                case 2: //Move above target state
                    {
                        if (MoveAbsolute(target + Vector3.up))
                            moveState = 3;
                    }
                    break;

                case 3: //Move down state
                    {
                        if (MoveRelative(Vector3.down))
                        {
                                moveState = 0;
                                moveTrigger = false;
                        }
                    }
                    break;

        }
             
	}

    bool MoveAbsolute (Vector3 localTarget)
    {
        if (moving == false)
        {
            current = transform.position;
        }
        
        moving = true;
        transform.Translate((localTarget - current) * Time.deltaTime * speed);

        if (Vector3.Distance(localTarget, transform.position) < 0.1f)
        {
            moving = false;
            return true;
        }

        return false;
    }

    bool MoveRelative (Vector3 localTarget)
    {

        if(moving == false)
        {
            current = transform.position;
        }

        moving = true;
        transform.Translate(localTarget * Time.deltaTime * speed);

        if (Vector3.Distance((localTarget + current), transform.position) < 0.1f)
        {
            moving = false;
            return true;
        }

        return false;
    }
}
