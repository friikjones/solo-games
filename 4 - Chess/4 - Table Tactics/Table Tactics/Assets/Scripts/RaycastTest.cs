﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RaycastTest : MonoBehaviour
{
    public GameObject pawn;
    public bool targetFound;
    public float distanceThreshold;

    private GameObject target;
    private NavMeshAgent pawnAgent;
    private Vector3 targetPosition;
    private Vector3 currentPosition;
    private Vector3 vertOffset;

    
    // Use this for initialization
    void Start()
    {
        targetFound = false;
        currentPosition = pawn.transform.position;
        pawnAgent = pawn.GetComponent<NavMeshAgent>();
        vertOffset = new Vector3(0, 1, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(targetFound == false)
        {
            FindTarget();
        }
        
        if(targetFound == true)
        {
            MoveToTarget();
        }

    }

    void FindTarget ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
                if (hit.collider != null && hit.collider.gameObject.tag == "Board")
                {
                    target = hit.collider.gameObject;
                    targetPosition = hit.collider.gameObject.transform.position;
                    Debug.Log(hit.collider.gameObject.name);
                    targetFound = true;
                }
        }
    }

    
    void MoveToTarget()
    {
        pawnAgent.SetDestination(targetPosition + vertOffset);

        if (CheckProximity(pawn.transform.position,(targetPosition),distanceThreshold))
        {
            target = null;
            targetFound = false;
            currentPosition = targetPosition;
            targetPosition = Vector3.zero;
        }

        
    }

    bool CheckProximity (Vector3 A, Vector3 B, float threshold)
    {
        float dist = Vector3.Distance(A, B);
        if (dist < threshold)
            return true;
        else
            return false;
    }
    
    void MoveUp()
    {
        pawnAgent.SetDestination(pawn.transform.position + vertOffset);
    }

    void MoveDown()
    {
        pawnAgent.SetDestination(pawn.transform.position - vertOffset);
    }

}
