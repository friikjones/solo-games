﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

public class Item {

    [XmlAttribute("Id")]
	public int Id;

    [XmlElement("X")]
    public float X;

	[XmlElement("Y")]
	public float Y;

	[XmlElement("Alpha")]
	public float Alpha;

	[XmlElement("Cost")]
	public int Cost;

	[XmlElement("Income")]
	public int Income;

	[XmlElement("N1")]
	public int N1;

	[XmlElement("N2")]
	public int N2;

	[XmlElement("N3")]
	public int N3;

	[XmlElement("N4")]
	public int N4;

	[XmlElement("N5")]
	public int N5;
	
	[XmlElement("Own")]
	public int Own;



}
