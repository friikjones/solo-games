﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnExitScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerExit(Collider other)
    {
        Debug.Log(other.gameObject.name);
        Destroy(other.gameObject);
    }

}
