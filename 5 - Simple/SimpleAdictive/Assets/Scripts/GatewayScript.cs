﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatewayScript : MonoBehaviour {

    private InstantiateHurdlesScript hurdlesScript;
    private bool crossed;
    
    // Use this for initialization
    void Start()
    {
        hurdlesScript = GameObject.Find("GameManager").GetComponent<InstantiateHurdlesScript>();
        crossed = false;
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(!crossed)
        {
            if (other.gameObject.tag.Equals("Player"))
            {
                hurdlesScript.hurdlePass = true;
                crossed = true;
            }
        }
        
        
    }
}
