﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerScript : MonoBehaviour {

    public bool jumpMidway;
    public bool jumpComplete;
    public float jumpForce;
    public float localGravity;


    private Rigidbody rb;
    private Vector3 currentPosition;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        jumpComplete = true;
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(rb.velocity);
        Physics.gravity = new Vector3(0, -localGravity, 0);

        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.velocity = Vector3.up * jumpForce;
            currentPosition = rb.position;
            //rb.useGravity = false;
            jumpComplete = false;
            jumpMidway = false;
        }

        if(rb.velocity.magnitude < 1)
        {
            jumpMidway = true;
        }

        if (Vector3.Distance(rb.position, currentPosition) < 0.01 && jumpMidway)
            jumpComplete = true;

        

    }
}
