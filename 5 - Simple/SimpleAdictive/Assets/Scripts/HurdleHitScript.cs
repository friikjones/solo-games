﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurdleHitScript : MonoBehaviour {

    private InstantiateHurdlesScript hurdlesScript;



	// Use this for initialization
	void Start () {
        hurdlesScript = GameObject.Find("GameManager").GetComponent<InstantiateHurdlesScript>();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        hurdlesScript.hurdleHit = true;
    }

}
