﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateHurdlesScript : MonoBehaviour {

    public bool hurdleHit;
    public bool hurdlePass;

    public GameObject player;
    public GameObject hurdle;

    public Vector3 hurdleOffset;

    public int hurdleCount;

	// Use this for initialization
	void Start () {
        hurdlePass = false ;
        hurdleCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
        if(hurdlePass == true)
        {
            GameObject clone = Instantiate(hurdle, this.transform) as GameObject;
            clone.transform.SetPositionAndRotation(player.transform.position + hurdleOffset, Quaternion.identity);
            hurdlePass = false;
            hurdleCount++;
        }

	}
}
