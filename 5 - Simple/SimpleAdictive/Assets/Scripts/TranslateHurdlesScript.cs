﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateHurdlesScript : MonoBehaviour {

    //public variables
    public Vector3 target;
    public float speed;
    public int moveState;
    public int leftMin, leftMax, rightMin, rightMax;

    //private variables
    private bool moving;
    private Vector3 current;
    private Vector3 localTarget;
    private Vector3 instantTarget;
    private int aux;

    void Start()
    {
        moving = false;
        moveState = 0;
    }


    void Update()
    {

        //State machine for linear movement
        switch (moveState)
        {
            
            case 0: //Idle state
                {
                    aux = Random.Range(-leftMax, -leftMin);
                    if (MoveRelative(new Vector3(aux, 0, 0)))
                        moveState = 1;
                }
                break;

            case 1: //Move up state
                {
                    if (MoveRelative(new Vector3(-aux, 0, 0)))
                        moveState = 2;
                }
                break;

            case 2:
                {
                    aux = Random.Range(rightMin, rightMax);
                    if (MoveRelative(new Vector3(aux, 0, 0)))
                        moveState = 3;
                }
                break;

            case 3:
                {
                    if (MoveRelative(new Vector3(-aux, 0, 0)))
                        moveState = 0;
                }
                break;

        }

    }

    bool MoveAbsolute(Vector3 localTarget)
    {
        if (moving == false)
        {
            current = transform.position;
        }

        moving = true;
        transform.Translate((localTarget - current) * Time.deltaTime * speed);

        if (Vector3.Distance(localTarget, transform.position) < 0.1f)
        {
            moving = false;
            return true;
        }

        return false;
    }


    bool MoveRelative(Vector3 localTarget)
    {

        if (moving == false)
        {
            current = transform.position;
        }

        moving = true;
        transform.Translate(localTarget * Time.deltaTime * speed);

        if (Vector3.Distance((localTarget + current), transform.position) < 0.1f)
        {
            moving = false;
            return true;
        }

        return false;
    }

}
