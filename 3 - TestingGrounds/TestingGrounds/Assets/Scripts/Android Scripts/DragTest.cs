﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragTest : MonoBehaviour {

    private Rigidbody2D rb;

    private Vector2 convertedTouch;

    private Vector2 startPos;
    private Vector2 direction;

    public int appWidth;
    public int appHeight;

    public float editorWidth;
    public float editorHeight;

    public float maxForce;
    public bool onGround;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        onGround = true;

    }

	
	// Update is called once per frame
	void Update ()
    {
        
        TouchControls();
        Debug.Log("Chao: " + onGround.ToString());

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            rb.velocity = Vector2.zero;
            onGround = true;
        }
    }


    void TouchPosition()
    {
        convertedTouch = Input.GetTouch(0).position;
        convertedTouch.y = (convertedTouch.y - appHeight / 2) * editorHeight/appHeight;
        convertedTouch.x = (convertedTouch.x - appWidth / 2) * editorWidth/appWidth;

        rb.transform.position = convertedTouch;

        Debug.Log("toque: " + Input.GetTouch(0).position);
        Debug.Log("convertido: " + convertedTouch);
    }

    void TouchControls()
    {
        if (Input.touchCount > 0 && onGround)
        {
            Touch touch = Input.GetTouch(0);
            
            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    startPos = touch.position;
                    //Debug.Log("start: " + touch.position);
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    // Determine direction by comparing the current touch position with the initial one
                    direction = touch.position - startPos;
                    //Debug.Log("direction: " + touch.position);
                    break;

                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    ConvertTouchToForce();
                    rb.AddForce(direction * maxForce);
                    Debug.Log("direction: " + direction);
                    onGround = false;
                    break;
            }

            
        }
    }

    void ConvertTouchToForce()
    {
        /*
        direction.y = direction.y * maxForce * 2 / appHeight;
        direction.x = direction.x * maxForce * 2 / appWidth;
        */

        direction = direction / Mathf.Sqrt((direction.x * direction.x) + (direction.y * direction.y));
        direction = direction * maxForce;

        if(direction.y <0)
        {
            direction = -direction;
        }

        if (direction.x > maxForce)
            direction.x = maxForce;
        if (direction.y > maxForce)
            direction.y = maxForce;

    }
}
