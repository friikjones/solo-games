﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawnerScript : MonoBehaviour {

    public float gridInc;
    public int gameState;
    public int level;
    public float yInit;

    private bool teste;

    public GameObject player;
    public GameObject cubeFab;

    private DragTest dragTest;
    private CubeScript cubeScript;

	// Use this for initialization
	void Start ()
    {
        cubeFab = this.transform.GetChild(0).gameObject;
        gameState = 3;
        level = 1;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
        if (Input.GetKeyDown(KeyCode.Space))
        {
            teste = true;
        }
        else
        {
            teste = false;
        }

        switch (gameState)
        {
            //no ar
            case 1:
                if (player.GetComponent<DragTest>().onGround == true)
                {
                    gameState = 2;
                }
                break;

            //esperando
            case 2:
                MoveAllDown();
                SpawnNext();
                level++;
                gameState = 3;
                break;

            //no chão
            case 3:
                if(CheckFinish(this.gameObject))
                {
                    gameState = 4;
                }
                if (player.GetComponent<DragTest>().onGround == false || teste)
                {
                    gameState = 1;
                }
                break;
            
            //fim de jogo
            case 4:
                Debug.Log("Fim");
                //Endgame
                break;

        }


	}

    void MoveDown (Transform target)
    {
        target.Translate(0, -gridInc, 0);
    }

    bool CheckFinish (GameObject spawner)
    {
        foreach (Transform child in spawner.transform)
        {
            if (child.GetComponent<CubeScript>().onEnd)
                return true;
        }
        return false;
    }

    void MoveAllDown ()
    {
        foreach (Transform child in this.transform)
        {
            MoveDown(child);
        }
    }

    void SpawnNext ()
    {
        GameObject instance =  Instantiate(cubeFab, this.transform);
        instance.transform.Translate(Random.Range(0,10)*gridInc , 0, 0);
    }

}
