﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraVerifyScript : MonoBehaviour {

    public Camera gameCamera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("gameCameraW: " + gameCamera.pixelWidth);
        Debug.Log("gameCameraWscaled: " + gameCamera.scaledPixelWidth);
        Debug.Log("gameCameraH: " + gameCamera.pixelHeight);
        Debug.Log("gameCameraHscaled: " + gameCamera.scaledPixelHeight);
    }
}
