﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeScript : MonoBehaviour {

    public int count;
    public bool onEnd = false;

    public GameObject objectSpawner;
    private ObjectSpawnerScript objectSpawnerScript;

	// Use this for initialization
	void Start () {
        objectSpawner = GameObject.Find("ObjectSpawner");
        count = objectSpawner.GetComponent<ObjectSpawnerScript>().level;
	}
	
	// Update is called once per frame
	void Update () {
        GetComponentInChildren<Text>().text = count.ToString();	
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EndGame")
            onEnd = true;
    }

}
