# Combat Tactics

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Combat Tactics](#combat-tactics)
	- [O Jogo](#o-jogo)
	- [Componentes](#componentes)
	- [Montagem](#montagem)
		- [A Tumba](#a-tumba)
		- [Os Heróis](#os-heris)
	- [Explorando a tumba](#explorando-a-tumba)

<!-- /TOC -->

## O Jogo

## Componentes

4 Heróis, cada um com:
  - Token
  - Ficha de Personagem
  - 2 Dados de 20 lados, um Vermelho e um Azul
  - 1 Dado de 12 lados, preto
  - **Arma e armadura de nível 0 `??`**

A Tumba, composta de:
  - 1 Peça de Início
  - `X` Peças de Nível 1
  - `X` Peças de Nível 2
  - `X` Peças de Nível 3
  - 1 Peça de Sarcófago

Cartas de Monstro, sendo:
 - `X` de Nível 1
 - `X` de Nível 2
 - `X` de Nível 3

Cartas de Tesouro, sendo:
  - `X` de Nível 1
  - `X` de Nível 2
  - `X` de Nível 3

## Montagem

### A Tumba

Coloque a Peça de Início virada para cima no centro da mesa.

Coloque a Peça de Sarcófago virada para baixo. Esse será a **Pilha de Exploração**.

Em seguida, embaralhe as peças de cada Nível da Tumba separadamente, e as coloque viradas para baixo sobre a Pilha de Exploração, na seguinte ordem:
- `X` Peças de Nível 3
- `X` Peças de Nível 2
- `X` Peças de Nível 1

Embaralhe as Cartas de Tesouro de cada Nível separadamente, e as coloque viradas para baixo, formando 3 pilhas diferentes, próximas a Pilha de Exploração. Faça o mesmo com as Cartas de Monstro.

![TableSetup](/Images/TableSetup.png)

### Os Heróis

Cada jogador escolhe um herói, e coloca o Token correspondente no Início da Tumba.

Cada jogador também coloca a ficha de Personagem em sua frente, montando da seguinte maneira:

![PlayerMat](/Images/PlayerMat.png)

## Explorando a tumba

Teste
