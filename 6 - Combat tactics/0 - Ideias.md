# Ideias

## Concepts

Skin para o jogo, exploradores modernos.
Sistema de atributos:
 - Aventureiro (AGI)
  - Movimentação
  - Armadilhas
 - Mercenário (STR)
  - Sobrevivência
  - Ataque bruto
 - Estudioso (INT)
  - Informação
  - Encantamentos

## Mecanicas

Sistema de batalha com Karma, com bônus
 - Fraquezas e forças estilo Ikaruga?

Sistema de batalha com equações
  - Como implementar?

Itens não identificados
  - Itens cruéis?

Permanência leve
  - Modificadores para os Heróis?
  - "Battle scars", estilo Guild of Dungeoneering
