<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [8 Regras para Roguelikes](#8-regras-para-roguelikes)
	- [1. No beheading rule](#1-no-beheading-rule)
	- [2. No cyanide rule](#2-no-cyanide-rule)
	- [3. Regras do item mascarado](#3-regras-do-item-mascarado)
	- [4. Regra de vantagem situacional de ID](#4-regra-de-vantagem-situacional-de-id)
	- [5. Regra do encantamento de items](#5-regra-do-encantamento-de-items)
	- [6. Regra da moeda de dois lados](#6-regra-da-moeda-de-dois-lados)
	- [7. Regra de reduzir Grind](#7-regra-de-reduzir-grind)
	- [8. Regra da corrida que você não pode ganhar](#8-regra-da-corrida-que-voc-no-pode-ganhar)

<!-- /TOC -->

# 8 Regras para Roguelikes

##  1. No beheading rule
  Nada pode matar o jogador de primeira, ou esperar algo que ele não tinha como saber.

##  2. No cyanide rule
  Nenhum item não identificado pode ser imediatamente fatal.

## 3. Regras do item mascarado
  Nenhum item pode ser dificil de identificar

  Adendo: Jogadores imergidos no jogo devem ter bônus quanto a isso. (Pc)

## 4. Regra de vantagem situacional de ID
  Efeitos de itens devem ser pensados de maneira que identificar os mesmos em algumas situações seja bom, e em outras mau.
  Se nenhuma poção dá dano, por exemplo, ela sempre será usada quando o jogador tiver poucos pontos de vida.

## 5. Regra do encantamento de items
  Sempre que possível, itens devem apresentar decisões interessantes

## 6. Regra da moeda de dois lados
  Dado conhecimento perfeito dos itens e seus usos, nenhum item deve ser completamente inútil
  Adendo: Itens ruins podem ser arremessados nos monstros, por exemplo

## 7. Regra de reduzir Grind
  O ato de gastar tempo deve depletar algum recurso finito, se modo a forçar os jogadores a limitar grind.

## 8. Regra da corrida que você não pode ganhar
  Monstros devem subir em dificuldade levemente mais rápido que o avanço dos jogadores, fazendo com que os mesmos devam usar táticas e itens para serem bem sucedidos
