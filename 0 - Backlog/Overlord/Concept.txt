[Ideia]

Jogo de Tabuleiro, de 3 a 5 jogadores
  Assimétrico
  1 jogador será o BBEG
  2 a 4 jogadores serão heróis

[GamePlay]

"Montagem"
  BBEG monta a dungeon usando tiles
    Guild of Dungeoneering *Referencia*
  Heróis escolhem a classe, com seu próprio Player Mat
    Black Plague *Referencia*
    Blood Rage *Referencia*

"Loop de Mecanica"
  'Turno do BBEG'
    Recolhe recursos dos monstros na mesa
    Compra monstros e skills do deck _?_
      * Possibilidade: Diferentes BBEGs
    Utiliza recursos para invocar mais monstros / usar skill / Upgrade para sí

  'Turno dos Herois'
    Reajeita equips e coisa assim
    Avança na dungeon
    Combate
    Loot

"Final Fight"
  No final, os jogadores encontram o BBEG, rola combate final.


[Meaningfull decisions]

"BBEG"
  * Investimento nos monstros
    - Recurso Vs Payback
    - Recurso Vs Machucar Heróis
  * Investimento em Upgrade
    - Tentar ficar mais forte para a batalha final
  * Investimento em skills
    - Enfraquecer Heróis

"Herois"
  * Utilização dos recursos
    - Matar monstros da maneira mais inteligente
    - Desviar de traps
    - /Outsmart/ BBEG

[Componentes]

"Na Mesa"
  Tiles estilo *Guild of Dungeoneering*
  BBEG Shield
    * Avaliar necessidade
  Miniaturas

"No Jogador"
  'BBEG'
    Deck de Cartas de monstros e skills
    Ficha do monstro
    Tokens de recurso
      * Tipos de recurso _?_

  'Herois'
    Ficha do Herói
      * Tokens para track de recurso _?_
    Cartas de skill
    Cartas de equips
