﻿using UnityEngine;
using System.Collections;

public class Movimentacao : MonoBehaviour {

	private Rigidbody core;
	public float speed;

	void Start() 
	{
		core = GetComponent<Rigidbody> ();
	}
	
	// FixedUpdate com física
	void FixedUpdate () 
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, 0.0f);

		core.AddForce (movement * speed);
	
	}
}
