﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public float steer;
	public float brake;
	private bool moving;

	private Rigidbody rb;

	// Use this for initialization
	void Start () 
	{
		rb = gameObject.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		CarControllerKeyboard (KeyCode.W, KeyCode.D, KeyCode.A, KeyCode.Space);

	}


	void CarControllerKeyboard(KeyCode Foward, KeyCode Right, KeyCode Left, KeyCode Brakes)
	{
		if(Input.GetKey(Foward))
		{
			rb.AddForce( -transform.right * speed);
			moving = true;
		}

		if(Input.GetKey(Brakes))
		{
			rb.AddForce (transform.right * brake);
			moving = true;
		}

		if(Input.GetKey(Right) && moving)
		{
			rb.angularVelocity = transform.up * steer /50;
		}

		if(Input.GetKey(Left) && moving)
		{
			rb.angularVelocity = -transform.up * steer/50;
		}

		moving = false;

	}


}
