﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

	public int fallCount;
	public GameObject Ball;

	public float timeCount;
	public float lastSpawn;

	// Use this for initialization
	void Start () {
	
		fallCount = 0;
		lastSpawn = 0;
		Instantiating ();
	}

	void Update ()
	{

		timeCount = Time.time - lastSpawn;
		Debug.Log (timeCount);
	}


	// Update is called once per frame
	void FixedUpdate () {
	


		if (timeCount > 4)
		{
			lastSpawn = Time.time;
			Instantiating ();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Ball") 
		{
			fallCount++;
			Destroy (other.gameObject);
		}
	}

	void Instantiating()
	{
		int Z = Random.Range (0,8);

		Instantiate(Ball, new Vector3 (Random.Range(-5f,5f),5f,Z-3), Quaternion.identity);
	}
}
