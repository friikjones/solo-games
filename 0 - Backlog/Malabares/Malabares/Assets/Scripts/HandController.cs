﻿using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {


	private Rigidbody rb1;
	private Rigidbody rb2;
	//private Vector3 velocityInitial;

	public float angle1;
	public float speedRatio1;
	private float speed1;
	public float radius1;
	public int z1;
	public float fineTuningX1;
	public float fineTuningY1;

	public float angle2;
	public float speedRatio2;
	private float speed2;
	public float radius2;
	public int z2;
	public float fineTuningX2;
	public float fineTuningY2;

	// Use this for initialization
	void Start () {
	
		//rb1 = GameObject.Find("Left").GetComponent<Rigidbody> ();
		rb1 = this.gameObject.transform.GetChild(0).GetComponent<Rigidbody> ();
		rb2 = this.gameObject.transform.GetChild(1).GetComponent<Rigidbody> ();


		rb1.position = new Vector3 (1, 1, 1);
		rb1.angularVelocity = Vector3.zero;
		rb1.rotation = Quaternion.identity;
		rb2.position = new Vector3 (-1, -1, -1);
		rb2.angularVelocity = Vector3.zero;
		rb2.rotation = Quaternion.identity;
		speed1 = (2 * Mathf.PI) / speedRatio1;
		speed2 = (2 * Mathf.PI) / speedRatio2;
	}
	
	// Update is called once per frame
	void Update () {
		HandMovementCircular (z1, angle1, speedRatio1, speed1 , radius1 , fineTuningX1 , fineTuningY1, rb1, KeyCode.A, KeyCode.D);
		HandMovementCircular (z2, angle2, speedRatio2, speed2 , radius2 , fineTuningX2 , fineTuningY2, rb2, KeyCode.J, KeyCode.L);
	}

	void HandMovementCircular (int z, float angle, float speedRatio, float speed, float radius, float fineTuningX, float fineTuningY,  Rigidbody rb, KeyCode left , KeyCode right)
	{
		rb.position = new Vector3 (fineTuningX + Mathf.Cos (angle) * radius, radius - fineTuningY + Mathf.Sin (angle) * radius, z);
		rb.rotation = Quaternion.Euler ( 0,0 , -90*(2.4f-angle));	

		//Debug.Log ("Angulo " + angle);
		if (Input.GetKey (left)) 
		{
			angle -= speed * Time.deltaTime;
		}
		else
		{
			if (Input.GetKey (right))
				angle += speed * Time.deltaTime;
		}		
	}
}
