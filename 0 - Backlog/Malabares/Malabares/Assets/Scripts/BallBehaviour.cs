﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour {


	public Vector3 calculationResults;


	//Variáveis da esfera
	private Rigidbody rb;
	public float handBounce;
	public float wallBounce;
	public float stdBounce;
	public float damper;

	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody> ();
	} 	
	
	// Update is called once per frame
	void Update () {
	
		//instantVelocity = rb.velocity.magnitude;

	}

	void OnCollisionEnter(Collision collision) 
	{
		float internalBounce;
		//Quaternion surfaceBounce;
		Vector3 surfaceBounce;


		ContactPoint contact = collision.contacts[0];
		surfaceBounce = collision.contacts [0].normal;
		//Debug.Log ("Angulo " +surfaceBounce.x + " " + surfaceBounce.y  + " " + surfaceBounce.z);

		if (collision.gameObject.tag == "Hand")
		{
			internalBounce = handBounce;
			rb.velocity = Vector3.Reflect (rb.velocity, surfaceBounce) * internalBounce;
			rb.velocity = new Vector3 (rb.velocity.x - rb.velocity.x * damper, rb.velocity.y, rb.velocity.z);
		}
		else
		{
			if (collision.gameObject.tag == "Wall")
			{
				internalBounce = wallBounce;
				rb.velocity = Vector3.Reflect (rb.velocity, surfaceBounce) * internalBounce;
				rb.velocity = new Vector3 (rb.velocity.x - rb.velocity.x * damper, rb.velocity.y, rb.velocity.z);
			}
			else
			{
				rb.velocity = new Vector3(-collision.relativeVelocity.x, collision.relativeVelocity.y/2,0);
				internalBounce = stdBounce;
		
			}
		}
	}


}
