﻿using UnityEngine;
using System.Collections;

public class HandBehaviour : MonoBehaviour {


	private Rigidbody rb;
	//private Vector3 velocityInitial;

	private bool direction;

	public float angle;
	public float speedRatio;
	private float speed;
	public float radius;

	public float fineTuningX;
	public float fineTuningY;



	// Use this for initialization
	void Start () {
	
		rb = GetComponent<Rigidbody> ();
		//velocityInitial = new Vector3(2, 0, 0);
		//rb.velocity = velocityInitial;
		rb.angularVelocity = Vector3.zero;
		rb.rotation = Quaternion.identity;
		rb.position = new Vector3 (rb.position.x, -4, 0);

		speed = (2 * Mathf.PI) / speedRatio;

	}
	
	// Update is called once per frame
	void Update () {
	

		HandMovementCircular (KeyCode.A, KeyCode.D);
		rb.ResetInertiaTensor();


	}

	void HandMovementCircular ( KeyCode left , KeyCode right)
	{
		rb.position = new Vector3 (fineTuningX + Mathf.Cos (angle) * radius, radius - fineTuningY + Mathf.Sin (angle) * radius, 0);
		rb.rotation = Quaternion.Euler ( 0,0 , -90*(2.4f-angle));

		//Debug.Log ("Angulo " + angle);
		if (Input.GetKey (left)) 
		{
			angle -= speed * Time.deltaTime;
		}
		else
		{
			if (Input.GetKey (right))
				angle += speed * Time.deltaTime;
		}		
	}

	void OnCollisionEnter() 
	{
		rb.ResetInertiaTensor();
		//rb.constraints = RigidbodyConstraints.FreezeAll;
		//rb.constraints = RigidbodyConstraints.None;

	}
}
