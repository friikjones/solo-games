﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMove : MonoBehaviour
{
	private Rigidbody rb;

    public int moveSpeed;
    public int controllerType;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controllerType = 1;
    }

    void Update()
    {

        if (controllerType == 1)
            MoveByKeys(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D);

    }

    void MoveByKeys(KeyCode Up, KeyCode Down, KeyCode Left, KeyCode Right)
    {

        if (Input.GetKey(Up))
        {
            transform.Translate(new Vector3(0, 0, 1) * moveSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(Down))
        {
            transform.Translate(new Vector3(0, 0, -1) * moveSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(Left))
        {
            transform.Translate(new Vector3(-1, 0, 0) * moveSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(Right))
        {
            transform.Translate(new Vector3(1, 0, 0) * moveSpeed * Time.deltaTime, Space.World);
        }
    }


}