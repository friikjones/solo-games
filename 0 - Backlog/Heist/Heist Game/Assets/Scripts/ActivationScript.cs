﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationScript : MonoBehaviour {

    //Private Variables
    private ActivationScript scriptExterno;
    private bool jogadorPresente;

    public KeyCode input;
    public GameObject Gatilho;
    public bool ativado;

    //Variaveis de controle
    public bool precisaJogador;
    public bool quandoPressionado;
    public bool umaVez;
    public bool porGatilho;




    // Use this for initialization
    void Start () {
        ativado = false;
        jogadorPresente = false;

        scriptExterno = Gatilho.GetComponent<ActivationScript>();
	}
	
	// Update is called once per frame
	void Update () {

        if(precisaJogador)
        {
            if(jogadorPresente)
            {
                Activations();
            }
            else
            {
                ativado = false;
            }
        }
        else
        {
            Activations();
        }

    }

    void ActivateWhilePressed ()
    {
        if (Input.GetKey(input))
        {
            ativado = true;
        }
        else
        {
            ativado = false;
        }

    }

    void ActivateOnce ()
    {
        if (Input.GetKey(input))
        {
            ativado = true;
        }

    }

    void ActivateByTrigger ()
    {
        ativado = scriptExterno.ativado;
    }

    void Activations()
    {
        if (quandoPressionado)
            ActivateWhilePressed();

        if (umaVez)
            ActivateOnce();

        if (porGatilho)
            ActivateByTrigger();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            jogadorPresente = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            jogadorPresente = false;
    }
}
