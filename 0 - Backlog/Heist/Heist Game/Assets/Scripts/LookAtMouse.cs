﻿using UnityEngine;
using System.Collections;

public class LookAtMouse : MonoBehaviour
{
	private Rigidbody rb;


	public float rotateSpeed;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void Update () 
	{
		LooAtMouse ();
		

	}
    

	void LooAtMouse ()
	{
		Plane playerPlane = new Plane(Vector3.up, transform.position);

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		float hitdist = 0.0f;
		if (playerPlane.Raycast (ray, out hitdist)) 
		{
			Vector3 targetPoint = ray.GetPoint(hitdist);

			Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
		}
	}
}