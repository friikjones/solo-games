﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject Player;
    public GameObject Target;

    public int cameraHeight;
    public bool followPlayer;
    public bool followMouse;
    public bool followTarget;
    public float mouseSensitivity;

    private float cameraX;
    private float cameraY;
    private float cameraZ;

    public Vector3 actionCamera;

    // Use this for initialization
    void Start () {
        actionCamera = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update () {

        //this.transform.position = new Vector3(Target.transform.position.x, cameraHeight, Target.transform.position.z);
        if(followMouse)
            CameraFollowMouse();
        if (followPlayer)
            CameraFollowPlayer();
        if (followTarget)
            CameraFollowTarget();


    }


    void RefreshCamera()
    {
        cameraX = cameraX + actionCamera.x;
        cameraY = cameraY + actionCamera.y;
        cameraZ = cameraZ + actionCamera.z;

        this.transform.position = new Vector3(cameraX, cameraY, cameraZ);
    }

    void CameraFollowPlayer()
    {
        cameraX = Player.transform.position.x;
        cameraY = cameraHeight;
        cameraZ = Player.transform.position.z;
        RefreshCamera();
    }

    void CameraFollowMouse()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        Vector3 targetPoint = new Vector3(
               Input.mousePosition.x,
               Input.mousePosition.y,
               Input.mousePosition.z
               );


        cameraX = (Player.transform.position.x + targetPoint.x/mouseSensitivity) / 2;
        cameraY = cameraHeight;
        cameraZ = (Player.transform.position.z + targetPoint.y/mouseSensitivity) / 2;
        
        RefreshCamera();
    }

    void CameraFollowTarget()
    {
        cameraX = (Player.transform.position.x + Target.transform.position.x) / 2;
        cameraY = cameraHeight;
        cameraZ = (Player.transform.position.z + Target.transform.position.z) / 2;

        RefreshCamera();
    }
}
