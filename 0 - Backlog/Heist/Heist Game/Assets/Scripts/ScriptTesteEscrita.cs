﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptTesteEscrita : MonoBehaviour {

    public CameraFollow scriptAlvo;

    private Vector3 aumento;

	// Use this for initialization
	void Start () {
        aumento = new Vector3 (0, 100, 0);
	}
	
	// Update is called once per frame
	void Update () {
        scriptAlvo = GameObject.Find("Main Camera").GetComponent<CameraFollow>();
        if (Input.GetKey(KeyCode.Space))
            scriptAlvo.actionCamera = aumento;
        else
            scriptAlvo.actionCamera = Vector3.zero;
	}
}
