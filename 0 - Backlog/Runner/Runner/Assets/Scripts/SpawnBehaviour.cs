﻿using UnityEngine;
using System.Collections;

public class SpawnBehaviour : MonoBehaviour {

	public GameObject Ground;
	private bool create;


	// Use this for initialization
	void Start () {
		create = false;
		//Instantiate(Ground, new Vector3 (0,-1,3), Quaternion.identity);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (create)
		{
			Instantiate(Ground, new Vector3 (0,-1,2), Quaternion.identity);
			create = false;
		}

			
	}

	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Ground")
		{
			create = true;
		}
	}
}
